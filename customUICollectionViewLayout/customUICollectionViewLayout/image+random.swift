//
//  image+random.swift
//  customUICollectionViewLayout
//
//  Created by Jerzyk on 13/02/2017.
//  Copyright © 2017 JS. All rights reserved.
//

import Foundation
import UIKit

extension UIImage {
    static func random() -> UIImage? {
        let randomNum = arc4random_uniform(11)
        return UIImage(named: "\(randomNum).jpeg")
    }
}
