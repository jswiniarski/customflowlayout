//
//  ViewController.swift
//  customUICollectionViewLayout
//
//  Created by Jerzyk on 08/02/2017.
//  Copyright © 2017 JS. All rights reserved.
//

import UIKit

let kAnimationDuration = 0.2

class ViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    enum layoutsAvailable {
        case grid
        case list
    }
    
    var currentLayout = layoutsAvailable.grid
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.applyCurrentLayout()
        
        collectionView.dataSource = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func changeLayoutButtonPressed(_ sender: Any) {
        if currentLayout == .grid {
            currentLayout = .list
        } else {
            currentLayout = .grid
        }
        UIView.animate(withDuration: kAnimationDuration) { () -> Void in
            self.applyCurrentLayout()
        }
    }
    
    private func applyCurrentLayout() {
        self.collectionView.collectionViewLayout.invalidateLayout()
        var layoutToApply: StretchyHeaderLayout
        switch currentLayout {
        case .grid:
            layoutToApply = GridFlowLayout()
            break
        case .list:
            layoutToApply = ListFlowLayout()
        default: //leaving default implementation in case somebody changes the layout enum
            layoutToApply = StretchyHeaderLayout()
        }
        self.collectionView.alwaysBounceVertical = true
        layoutToApply.headerReferenceSize = CGSize(width: 320, height: 60)
        
                self.collectionView.register(UICollectionReusableView.classForCoder(), forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "headerID")
        
        
        self.collectionView.setCollectionViewLayout(layoutToApply, animated: true)
    }
}

extension ViewController: UICollectionViewDelegate {
    
}

extension ViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let header = self.collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "headerID", for: indexPath)
        
        let bounds = header.bounds
        let imageView = UIImageView(frame: bounds)
        imageView.image = UIImage(named: "5.jpeg")
        
        imageView.contentMode = UIViewContentMode.scaleAspectFill
        imageView.clipsToBounds = true
        imageView.autoresizingMask = UIViewAutoresizing.flexibleHeight
        
        header.addSubview(imageView)
        
        return header
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 17
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: TestUICollectionViewCell.cellIdentifier,
                                                      for: indexPath) as! TestUICollectionViewCell
        cell.mainLabel.text = "\(indexPath.section)-\(indexPath.row)"
        cell.imageView.image = UIImage(named: "\(indexPath.row).jpeg")
        return cell
    }
}
