//
//  StickyHeaderLayout.swift
//  customUICollectionViewLayout
//
//  Created by Jerzyk on 13/02/2017.
//  Copyright © 2017 JS. All rights reserved.
//

import Foundation
import UIKit

class StretchyHeaderLayout: UICollectionViewFlowLayout {
    
    override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
        return true
    }
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        guard let collectionView = self.collectionView else {
            return nil
        }
        
        let insets = collectionView.contentInset
        let offset = collectionView.contentOffset
        
        let minY = -insets.top
        
        guard let attributes = super.layoutAttributesForElements(in: rect) else {
            return nil
        }
        
        if (offset.y < minY) {
            let deltaY = fabsf(Float(offset.y) - Float(minY))
            
            for attr in attributes { //as! UICollectionViewLayoutAttributes ?
                if attr.representedElementKind == UICollectionElementKindSectionHeader {
                    let headerSize = self.headerReferenceSize
                    var headerRect = attr.frame
                    headerRect.size.height = max(minY, headerSize.height + CGFloat(deltaY))
                    headerRect.origin.y = CGFloat(headerRect.origin.y) - CGFloat(deltaY)
                    attr.frame = headerRect
                    break
                }
            }
        }
        return attributes
    }
}
