//
//  TestUICollectionViewCell.swift
//  customUICollectionViewLayout
//
//  Created by Jerzyk on 08/02/2017.
//  Copyright © 2017 JS. All rights reserved.
//

import Foundation
import UIKit

class TestUICollectionViewCell: UICollectionViewCell {
    static let cellIdentifier = "TestCellID"
    
    @IBOutlet weak var mainLabel: UILabel!
    private var oneTimeSetup = false
    @IBOutlet weak var imageView: UIImageView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        
    }
}
